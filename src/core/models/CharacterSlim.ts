export default interface CharacterSlim {
    characterId: number;
    name: string;
}