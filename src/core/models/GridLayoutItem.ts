export default interface GridLayoutItem {
    key: string;
    layout: object;
}