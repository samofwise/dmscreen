import Character from "./Character";

export default interface DdbApiMessage {
    data: Character;
}