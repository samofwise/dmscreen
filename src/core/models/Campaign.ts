import CharacterSlim from "./CharacterSlim";

export default interface Campaign {
    name: string;
    characters: CharacterSlim[];
}
