export default interface InventoryItem {
	definition: ItemDefinition;
	
}

export interface ItemDefinition {
	name: string;
	magic: boolean;

}
