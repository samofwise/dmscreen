import Campaign from './Campaign';
import Class from './Classes';
import InventoryItem from './InventoryItem';

export default interface Character {
	id: number;
	name: string;
	avatarUrl: string;
	frameAvatarUrl: string;
	readonlyUrl: string;
	campaign?: Campaign;
	temporaryHitPoints: number;
	baseHitPoints: number;
	overrideHitPoints: number;
	bonusHitPoints: number;
	removedHitPoints: number;
	stats: Stat[];
	classes: Class[];
	inventory: InventoryItem[];
}

export interface Stat {
	id: number;
	value: number;
}
