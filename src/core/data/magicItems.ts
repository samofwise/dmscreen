const magicItems = [
	{
		type: 'Common - Minor',
		items: [
			{
				name: 'Potion of Healing',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-healing',
				alwaysInclude: true
			},
			{ name: 'Armor of gleaming', link: 'https://www.dndbeyond.com/magic-items/armor-of-gleaming' },
			{ name: 'Bead of nourishment', link: 'https://www.dndbeyond.com/magic-items/bead-of-nourishment' },
			{ name: 'Bead of refreshment', link: 'https://www.dndbeyond.com/magic-items/bead-of-refreshment' },
			{ name: 'Boots of false tracks', link: 'https://www.dndbeyond.com/magic-items/boots-of-false-tracks' },
			{ name: 'Candle of the deep', link: 'https://www.dndbeyond.com/magic-items/candle-of-the-deep' },
			{ name: 'Cast-off armor', link: 'https://www.dndbeyond.com/magic-items/cast-off-armor' },
			{ name: 'Charlatan’s die', link: 'https://www.dndbeyond.com/magic-items/charlatans-die' },
			{ name: 'Cloak of billowing', link: 'https://www.dndbeyond.com/magic-items/cloak-of-billowing' },
			{ name: 'Cloak of many fashions', link: 'https://www.dndbeyond.com/magic-items/cloak-of-many-fashions' },
			{ name: 'Clockwork amulet', link: 'https://www.dndbeyond.com/magic-items/clockwork-amulet' },
			{ name: 'Clothes of mending', link: 'https://www.dndbeyond.com/magic-items/clothes-of-mending' },
			{ name: 'Dark shard amulet', link: 'https://www.dndbeyond.com/magic-items/dark-shard-amulet' },
			{ name: 'Dread helm', link: 'https://www.dndbeyond.com/magic-items/dread-helm' },
			{ name: 'Ear horn of hearing', link: 'https://www.dndbeyond.com/magic-items/ear-horn-of-hearing' },
			{ name: 'Enduring spellbook', link: 'https://www.dndbeyond.com/magic-items/enduring-spellbook' },
			{ name: 'Ersatz eye', link: 'https://www.dndbeyond.com/magic-items/ersatz-eye' },
			{ name: 'Hat of vermin', link: 'https://www.dndbeyond.com/magic-items/hat-of-vermin' },
			{ name: 'Hat of wizardry', link: 'https://www.dndbeyond.com/magic-items/hat-of-wizardry' },
			{ name: 'Heward’s handy spice pouch', link: 'https://www.dndbeyond.com/magic-items/hewards-handy-spice-pouch' },
			{ name: 'Horn of silent alarm', link: 'https://www.dndbeyond.com/magic-items/horn-of-silent-alarm' },
			{ name: 'Instrument of illusions', link: 'https://www.dndbeyond.com/magic-items/instrument-of-illusions' },
			{ name: 'Instrument of scribing', link: 'https://www.dndbeyond.com/magic-items/instrument-of-scribing' },
			{ name: 'Lock of trickery', link: 'https://www.dndbeyond.com/magic-items/lock-of-trickery' },
			{ name: 'Moon-touched sword', link: 'https://www.dndbeyond.com/magic-items/moon-touched-sword' },
			{ name: 'Mystery key', link: 'https://www.dndbeyond.com/magic-items/mystery-key' },
			{ name: 'Orb of direction', link: 'https://www.dndbeyond.com/magic-items/orb-of-direction' },
			{ name: 'Orb of time', link: 'https://www.dndbeyond.com/magic-items/orb-of-time' },
			{ name: 'Perfume of bewitching', link: 'https://www.dndbeyond.com/magic-items/perfume-of-bewitching' },
			{ name: 'Pipe of smoke monsters', link: 'https://www.dndbeyond.com/magic-items/pipe-of-smoke-monsters' },
			{ name: 'Pole of angling', link: 'https://www.dndbeyond.com/magic-items/pole-of-angling' },
			{ name: 'Pole of collapsing', link: 'https://www.dndbeyond.com/magic-items/pole-of-collapsing' },
			{ name: 'Pot of awakening', link: 'https://www.dndbeyond.com/magic-items/pot-of-awakening' },
			{ name: 'Potion of climbing', link: 'https://www.dndbeyond.com/magic-items/potion-of-climbing' },
			{ name: 'Rope of mending', link: 'https://www.dndbeyond.com/magic-items/rope-of-mending' },
			{ name: 'Ruby of the war mage', link: 'https://www.dndbeyond.com/magic-items/ruby-of-the-war-mage' },
			{ name: 'Shield of expression', link: 'https://www.dndbeyond.com/magic-items/shield-of-expression' },
			{ name: 'Smoldering armor', link: 'https://www.dndbeyond.com/magic-items/smoldering-armor' },
			{ name: 'Spell scroll (cantrip)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Spell scroll (1st level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Staff of adornment', link: 'https://www.dndbeyond.com/magic-items/staff-of-adornment' },
			{ name: 'Staff of birdcalls', link: 'https://www.dndbeyond.com/magic-items/staff-of-birdcalls' },
			{ name: 'Staff of flowers', link: 'https://www.dndbeyond.com/magic-items/staff-of-flowers' },
			{ name: 'Talking doll', link: 'https://www.dndbeyond.com/magic-items/talking-doll' },
			{ name: 'Tankard of sobriety', link: 'https://www.dndbeyond.com/magic-items/tankard-of-sobriety' },
			{ name: 'Unbreakable arrow', link: 'https://www.dndbeyond.com/magic-items/unbreakable-arrow' },
			{ name: 'Veteran’s cane', link: 'https://www.dndbeyond.com/magic-items/veterans-cane' },
			{ name: 'Walloping ammunition', link: 'https://www.dndbeyond.com/magic-items/walloping-ammunition' },
			{ name: 'Wand of conducting', link: 'https://www.dndbeyond.com/magic-items/wand-of-conducting' },
			{ name: 'Wand of pyrotechnics', link: 'https://www.dndbeyond.com/magic-items/wand-of-pyrotechnics' },
			{ name: 'Wand of scowls', link: 'https://www.dndbeyond.com/magic-items/wand-of-scowls' },
			{ name: 'Wand of smiles', link: 'https://www.dndbeyond.com/magic-items/wand-of-smiles' }
		]
	},
	{
		type: 'Uncommon - Minor',
		items: [
			{
				name: 'Potion of Healing (Greater)',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-healing-greater',
				alwaysInclude: true
			},
			{ name: 'Alchemy jug', link: 'https://www.dndbeyond.com/magic-items/alchemy-jug' },
			{ name: 'Ammunition, +1', link: 'https://www.dndbeyond.com/magic-items/ammunition-1' },
			{ name: 'Bag of holding', link: 'https://www.dndbeyond.com/magic-items/bag-of-holding' },
			{ name: 'Cap of water breathing', link: 'https://www.dndbeyond.com/magic-items/cap-of-water-breathing' },
			{ name: 'Cloak of the manta ray', link: 'https://www.dndbeyond.com/magic-items/cloak-of-the-manta-ray' },
			{ name: 'Decanter of endless water', link: 'https://www.dndbeyond.com/magic-items/decanter-of-endless-water' },
			{ name: 'Driftglobe', link: 'https://www.dndbeyond.com/magic-items/driftglobe' },
			{ name: 'Dust of disappearance', link: 'https://www.dndbeyond.com/magic-items/dust-of-disappearance' },
			{ name: 'Dust of dryness', link: 'https://www.dndbeyond.com/magic-items/dust-of-dryness' },
			{
				name: 'Dust of sneezing and choking',
				link: 'https://www.dndbeyond.com/magic-items/dust-of-sneezing-and-choking'
			},
			{ name: 'Elemental gem', link: 'https://www.dndbeyond.com/magic-items/elemental-gem' },
			{ name: 'Eyes of minute seeing', link: 'https://www.dndbeyond.com/magic-items/eyes-of-minute-seeing' },
			{ name: 'Goggles of night', link: 'https://www.dndbeyond.com/magic-items/goggles-of-night' },
			{
				name: 'Helm of comprehending languages',
				link: 'https://www.dndbeyond.com/magic-items/helm-of-comprehending-languages'
			},
			{ name: 'Immovable rod', link: 'https://www.dndbeyond.com/magic-items/immovable-rod' },
			{ name: 'Keoghtom’s ointment', link: 'https://www.dndbeyond.com/magic-items/keoghtoms-ointment' },
			{ name: 'Lantern of revealing', link: 'https://www.dndbeyond.com/magic-items/lantern-of-revealing' },
			{ name: 'Mariner’s armor', link: 'https://www.dndbeyond.com/magic-items/mariners-armor' },
			{ name: 'Mithral armor', link: 'https://www.dndbeyond.com/magic-items/mithral-armor' },
			{ name: 'Oil of slipperiness', link: 'https://www.dndbeyond.com/magic-items/oil-of-slipperiness' },
			{ name: 'Periapt of health', link: 'https://www.dndbeyond.com/magic-items/periapt-of-health' },
			{ name: 'Philter of love', link: 'https://www.dndbeyond.com/magic-items/philter-of-love' },
			{
				name: 'Potion of animal friendship',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-animal-friendship'
			},
			{ name: 'Potion of fire breath', link: 'https://www.dndbeyond.com/magic-items/potion-of-fire-breath' },
			{ name: 'Potion of growth', link: 'https://www.dndbeyond.com/magic-items/potion-of-growth' },
			{
				name: 'Potion of hill giant strength',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-hill-giant-strength'
			},
			{ name: 'Potion of poison', link: 'https://www.dndbeyond.com/magic-items/potion-of-poison' },
			{ name: 'Potion of resistance', link: 'https://www.dndbeyond.com/magic-items/potion-of-resistance' },
			{ name: 'Potion of water breathing', link: 'https://www.dndbeyond.com/magic-items/potion-of-water-breathing' },
			{ name: 'Ring of swimming', link: 'https://www.dndbeyond.com/magic-items/ring-of-swimming' },
			{ name: 'Robe of useful items', link: 'https://www.dndbeyond.com/magic-items/robe-of-useful-items' },
			{ name: 'Rope of climbing', link: 'https://www.dndbeyond.com/magic-items/rope-of-climbing' },
			{ name: 'Saddle of the cavalier', link: 'https://www.dndbeyond.com/magic-items/saddle-of-the-cavalier' },
			{ name: 'Sending stones', link: 'https://www.dndbeyond.com/magic-items/sending-stones' },
			{ name: 'Spell scroll (2nd level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Spell scroll (3rd level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Wand of magic detection', link: 'https://www.dndbeyond.com/magic-items/wand-of-magic-detection' },
			{ name: 'Wand of secrets', link: 'https://www.dndbeyond.com/magic-items/wand-of-secrets' }
		]
	},
	{
		type: 'Uncommon - Major',
		items: [
			{ name: 'Adamantine armor', link: 'https://www.dndbeyond.com/magic-items/adamantine-armor' },
			{
				name: 'Amulet of proof against detection and location',
				link: 'https://www.dndbeyond.com/magic-items/amulet-of-proof-against-detection-and-location'
			},
			{ name: 'Bag of tricks', link: 'https://www.dndbeyond.com/magic-items/bag-of-tricks' },
			{ name: 'Boots of elvenkind', link: 'https://www.dndbeyond.com/magic-items/boots-of-elvenkind' },
			{
				name: 'Boots of striding and springing',
				link: 'https://www.dndbeyond.com/magic-items/boots-of-striding-and-springing'
			},
			{ name: 'Boots of the winterlands', link: 'https://www.dndbeyond.com/magic-items/boots-of-the-winterlands' },
			{ name: 'Bracers of archery', link: 'https://www.dndbeyond.com/magic-items/bracers-of-archery' },
			{ name: 'Brooch of shielding', link: 'https://www.dndbeyond.com/magic-items/brooch-of-shielding' },
			{ name: 'Broom of flying', link: 'https://www.dndbeyond.com/magic-items/broom-of-flying' },
			{ name: 'Circlet of blasting', link: 'https://www.dndbeyond.com/magic-items/circlet-of-blasting' },
			{ name: 'Cloak of elvenkind', link: 'https://www.dndbeyond.com/magic-items/cloak-of-elvenkind' },
			{ name: 'Cloak of protection', link: 'https://www.dndbeyond.com/magic-items/cloak-of-protection' },
			{ name: 'Deck of illusions', link: 'https://www.dndbeyond.com/magic-items/deck-of-illusions' },
			{ name: 'Eversmoking bottle', link: 'https://www.dndbeyond.com/magic-items/eversmoking-bottle' },
			{ name: 'Eyes of charming', link: 'https://www.dndbeyond.com/magic-items/eyes-of-charming' },
			{ name: 'Eyes of the eagle', link: 'https://www.dndbeyond.com/magic-items/eyes-of-the-eagle' },
			{
				name: 'Figurine of wondrous power (silver raven)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{ name: 'Gauntlets of ogre power', link: 'https://www.dndbeyond.com/magic-items/gauntlets-of-ogre-power' },
			{ name: 'Gem of brightness', link: 'https://www.dndbeyond.com/magic-items/gem-of-brightness' },
			{ name: 'Gloves of missile snaring', link: 'https://www.dndbeyond.com/magic-items/gloves-of-missile-snaring' },
			{
				name: 'Gloves of swimming and climbing',
				link: 'https://www.dndbeyond.com/magic-items/gloves-of-swimming-and-climbing'
			},
			{ name: 'Gloves of thievery', link: 'https://www.dndbeyond.com/magic-items/gloves-of-thievery' },
			{ name: 'Hat of disguise', link: 'https://www.dndbeyond.com/magic-items/hat-of-disguise' },
			{ name: 'Headband of intellect', link: 'https://www.dndbeyond.com/magic-items/headband-of-intellect' },
			{ name: 'Helm of telepathy', link: 'https://www.dndbeyond.com/magic-items/helm-of-telepathy' },
			{
				name: 'Instrument of the bards (Doss lute)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{
				name: 'Instrument of the bards (Fochlucan bandore)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{
				name: 'Instrument of the bards (Mac-Fuirmidh cittern)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{ name: 'Javelin of lightning', link: 'https://www.dndbeyond.com/magic-items/javelin-of-lightning' },
			{ name: 'Medallion of thoughts', link: 'https://www.dndbeyond.com/magic-items/medallion-of-thoughts' },
			{ name: 'Necklace of adaptation', link: 'https://www.dndbeyond.com/magic-items/necklace-of-adaptation' },
			{ name: 'Pearl of power', link: 'https://www.dndbeyond.com/magic-items/pearl-of-power' },
			{ name: 'Periapt of wound closure', link: 'https://www.dndbeyond.com/magic-items/periapt-of-wound-closure' },
			{ name: 'Pipes of haunting', link: 'https://www.dndbeyond.com/magic-items/pipes-of-haunting' },
			{ name: 'Pipes of the sewers', link: 'https://www.dndbeyond.com/magic-items/pipes-of-the-sewers' },
			{ name: 'Quiver of Ehlonna', link: 'https://www.dndbeyond.com/magic-items/quiver-of-ehlonna' },
			{ name: 'Ring of jumping', link: 'https://www.dndbeyond.com/magic-items/ring-of-jumping' },
			{ name: 'Ring of mind shielding', link: 'https://www.dndbeyond.com/magic-items/ring-of-mind-shielding' },
			{ name: 'Ring of warmth', link: 'https://www.dndbeyond.com/magic-items/ring-of-warmth' },
			{ name: 'Ring of water walking', link: 'https://www.dndbeyond.com/magic-items/ring-of-water-walking' },
			{ name: 'Rod of the pact keeper, +1', link: 'https://www.dndbeyond.com/magic-items/rod-of-the-pact-keeper-1' },
			{ name: 'Sentinel shield', link: 'https://www.dndbeyond.com/magic-items/sentinel-shield' },
			{ name: 'Shield, +1', link: 'https://www.dndbeyond.com/magic-items/shield-1' },
			{
				name: 'Slippers of spider climbing',
				link: 'https://www.dndbeyond.com/magic-items/slippers-of-spider-climbing'
			},
			{ name: 'Staff of the adder', link: 'https://www.dndbeyond.com/magic-items/staff-of-the-adder' },
			{ name: 'Staff of the python', link: 'https://www.dndbeyond.com/magic-items/staff-of-the-python' },
			{
				name: 'Stone of good luck (luckstone)',
				link: 'https://www.dndbeyond.com/magic-items/stone-of-good-luck-luckstone'
			},
			{ name: 'Sword of vengeance', link: 'https://www.dndbeyond.com/magic-items/sword-of-vengeance' },
			{ name: 'Trident of fish command', link: 'https://www.dndbeyond.com/magic-items/trident-of-fish-command' },
			{ name: 'Wand of magic missiles', link: 'https://www.dndbeyond.com/magic-items/wand-of-magic-missiles' },
			{ name: 'Wand of the war mage, +1', link: 'https://www.dndbeyond.com/magic-items/wand-of-the-war-mage-1' },
			{ name: 'Wand of web', link: 'https://www.dndbeyond.com/magic-items/wand-of-web' },
			{ name: 'Weapon of warning', link: 'https://www.dndbeyond.com/magic-items/weapon-of-warning' },
			{ name: 'Weapon, +1', link: 'https://www.dndbeyond.com/magic-items/weapon-1' },
			{ name: 'Wind fan', link: 'https://www.dndbeyond.com/magic-items/wind-fan' },
			{ name: 'Winged boots', link: 'https://www.dndbeyond.com/magic-items/winged-boots' }
		]
	},
	{
		type: 'Rare - Minor',
		items: [
			{
				name: 'Potion of Healing (Superior)',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-healing-superior',
				alwaysInclude: true
			},
			{ name: 'Ammunition, +2', link: 'https://www.dndbeyond.com/magic-items/ammunition-2' },
			{ name: 'Bag of beans', link: 'https://www.dndbeyond.com/magic-items/bag-of-beans' },
			{ name: 'Bead of force', link: 'https://www.dndbeyond.com/magic-items/bead-of-force' },
			{ name: 'Chime of opening', link: 'https://www.dndbeyond.com/magic-items/chime-of-opening' },
			{ name: 'Elixir of health', link: 'https://www.dndbeyond.com/magic-items/elixir-of-health' },
			{ name: 'Folding boat', link: 'https://www.dndbeyond.com/magic-items/folding-boat' },
			{ name: 'Heward’s handy haversack', link: 'https://www.dndbeyond.com/magic-items/hewards-handy-haversack' },
			{ name: 'Horseshoes of speed', link: 'https://www.dndbeyond.com/magic-items/horseshoes-of-speed' },
			{ name: 'Necklace of fireballs', link: 'https://www.dndbeyond.com/magic-items/necklace-of-fireballs' },
			{ name: 'Oil of etherealness', link: 'https://www.dndbeyond.com/magic-items/oil-of-etherealness' },
			{ name: 'Portable hole', link: 'https://www.dndbeyond.com/magic-items/portable-hole' },
			{ name: 'Potion of clairvoyance', link: 'https://www.dndbeyond.com/magic-items/potion-of-clairvoyance' },
			{ name: 'Potion of diminution', link: 'https://www.dndbeyond.com/magic-items/potion-of-diminution' },
			{
				name: 'Potion of fire giant strength',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-fire-giant-strength'
			},
			{
				name: 'Potion of frost giant strength',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-frost-giant-strength'
			},
			{ name: 'Potion of gaseous form', link: 'https://www.dndbeyond.com/magic-items/potion-of-gaseous-form' },
			{ name: 'Potion of heroism', link: 'https://www.dndbeyond.com/magic-items/potion-of-heroism' },
			{ name: 'Potion of invulnerability', link: 'https://www.dndbeyond.com/magic-items/potion-of-invulnerability' },
			{ name: 'Potion of mind reading', link: 'https://www.dndbeyond.com/magic-items/potion-of-mind-reading' },
			{
				name: 'Potion of stone giant strength',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-stone-giant-strength'
			},
			{ name: 'Quaal’s feather token', link: 'https://www.dndbeyond.com/magic-items/quaals-feather-token' },
			{ name: 'Scroll of protection', link: 'https://www.dndbeyond.com/magic-items/scroll-of-protection' },
			{ name: 'Spell scroll (4th level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Spell scroll (5th level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' }
		]
	},
	{
		type: 'Rare - Major',
		items: [
			{ name: 'Amulet of health', link: 'https://www.dndbeyond.com/magic-items/amulet-of-health' },
			{ name: 'Armor of resistance', link: 'https://www.dndbeyond.com/magic-items/armor-of-resistance' },
			{ name: 'Armor of vulnerability', link: 'https://www.dndbeyond.com/magic-items/armor-of-vulnerability' },
			{ name: 'Armor, +1', link: 'https://www.dndbeyond.com/magic-items/armor-1' },
			{ name: 'Arrow-catching shield', link: 'https://www.dndbeyond.com/magic-items/arrow-catching-shield' },
			{ name: 'Belt of dwarvenkind', link: 'https://www.dndbeyond.com/magic-items/belt-of-dwarvenkind' },
			{
				name: 'Belt of hill giant strength',
				link: 'https://www.dndbeyond.com/magic-items/belt-of-hill-giant-strength'
			},
			{ name: 'Berserker axe', link: 'https://www.dndbeyond.com/magic-items/berserker-axe' },
			{ name: 'Boots of levitation', link: 'https://www.dndbeyond.com/magic-items/boots-of-levitation' },
			{ name: 'Boots of speed', link: 'https://www.dndbeyond.com/magic-items/boots-of-speed' },
			{
				name: 'Bowl of commanding water elementals',
				link: 'https://www.dndbeyond.com/magic-items/bowl-of-commanding-water-elementals'
			},
			{ name: 'Bracers of defense', link: 'https://www.dndbeyond.com/magic-items/bracers-of-defense' },
			{
				name: 'Brazier of commanding fire elementals',
				link: 'https://www.dndbeyond.com/magic-items/brazier-of-commanding-fire-elementals'
			},
			{ name: 'Cape of the mountebank', link: 'https://www.dndbeyond.com/magic-items/cape-of-the-mountebank' },
			{
				name: 'Censer of controlling air elementals',
				link: 'https://www.dndbeyond.com/magic-items/censer-of-controlling-air-elementals'
			},
			{ name: 'Cloak of displacement', link: 'https://www.dndbeyond.com/magic-items/cloak-of-displacement' },
			{ name: 'Cloak of the bat', link: 'https://www.dndbeyond.com/magic-items/cloak-of-the-bat' },
			{ name: 'Cube of force', link: 'https://www.dndbeyond.com/magic-items/cube-of-force' },
			{ name: 'Daern’s instant fortress', link: 'https://www.dndbeyond.com/magic-items/daerns-instant-fortress' },
			{ name: 'Dagger of venom', link: 'https://www.dndbeyond.com/magic-items/dagger-of-venom' },
			{ name: 'Dimensional shackles', link: 'https://www.dndbeyond.com/magic-items/dimensional-shackles' },
			{ name: 'Dragon slayer', link: 'https://www.dndbeyond.com/magic-items/dragon-slayer' },
			{ name: 'Elven chain', link: 'https://www.dndbeyond.com/magic-items/elven-chain' },
			{
				name: 'Figurine of wondrous power (bronze griffon)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{
				name: 'Figurine of wondrous power (ebony fly)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{
				name: 'Figurine of wondrous power (golden lions)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{
				name: 'Figurine of wondrous power (ivory goats)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{
				name: 'Figurine of wondrous power (marble elephant)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{
				name: 'Figurine of wondrous power (onyx dog)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{
				name: 'Figurine of wondrous power (serpentine owl)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{ name: 'Flame tongue', link: 'https://www.dndbeyond.com/magic-items/flame-tongue' },
			{ name: 'Gem of seeing', link: 'https://www.dndbeyond.com/magic-items/gem-of-seeing' },
			{ name: 'Giant slayer', link: 'https://www.dndbeyond.com/magic-items/giant-slayer' },
			{ name: 'Glamoured studded leather', link: 'https://www.dndbeyond.com/magic-items/glamoured-studded-leather' },
			{ name: 'Helm of teleportation', link: 'https://www.dndbeyond.com/magic-items/helm-of-teleportation' },
			{ name: 'Horn of blasting', link: 'https://www.dndbeyond.com/magic-items/horn-of-blasting' },
			{ name: 'Horn of Valhalla (silver or brass)', link: 'https://www.dndbeyond.com/magic-items/horn-of-valhalla' },
			{
				name: 'Instrument of the bards (Canaith mandolin)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{
				name: 'Instrument of the bards (Cli lyre)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{ name: 'Ioun stone (awareness)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (protection)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (reserve)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (sustenance)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Iron bands of Bilarro', link: 'https://www.dndbeyond.com/magic-items/iron-bands-of-bilarro' },
			{ name: 'Mace of disruption', link: 'https://www.dndbeyond.com/magic-items/mace-of-disruption' },
			{ name: 'Mace of smiting', link: 'https://www.dndbeyond.com/magic-items/mace-of-smiting' },
			{ name: 'Mace of terror', link: 'https://www.dndbeyond.com/magic-items/mace-of-terror' },
			{ name: 'Mantle of spell resistance', link: 'https://www.dndbeyond.com/magic-items/mantle-of-spell-resistance' },
			{ name: 'Necklace of prayer beads', link: 'https://www.dndbeyond.com/magic-items/necklace-of-prayer-beads' },
			{
				name: 'Periapt of proof against poison',
				link: 'https://www.dndbeyond.com/magic-items/periapt-of-proof-against-poison'
			},
			{ name: 'Ring of animal influence', link: 'https://www.dndbeyond.com/magic-items/ring-of-animal-influence' },
			{ name: 'Ring of evasion', link: 'https://www.dndbeyond.com/magic-items/ring-of-evasion' },
			{ name: 'Ring of feather falling', link: 'https://www.dndbeyond.com/magic-items/ring-of-feather-falling' },
			{ name: 'Ring of free action', link: 'https://www.dndbeyond.com/magic-items/ring-of-free-action' },
			{ name: 'Ring of protection', link: 'https://www.dndbeyond.com/magic-items/ring-of-protection' },
			{ name: 'Ring of resistance', link: 'https://www.dndbeyond.com/magic-items/ring-of-resistance' },
			{ name: 'Ring of spell storing', link: 'https://www.dndbeyond.com/magic-items/ring-of-spell-storing' },
			{ name: 'Ring of the ram', link: 'https://www.dndbeyond.com/magic-items/ring-of-the-ram' },
			{ name: 'Ring of X-ray vision', link: 'https://www.dndbeyond.com/magic-items/ring-of-x-ray-vision' },
			{ name: 'Robe of eyes', link: 'https://www.dndbeyond.com/magic-items/robe-of-eyes' },
			{ name: 'Rod of rulership', link: 'https://www.dndbeyond.com/magic-items/rod-of-rulership' },
			{ name: 'Rod of the pact keeper, +2', link: 'https://www.dndbeyond.com/magic-items/rod-of-the-pact-keeper-2' },
			{ name: 'Rope of entanglement', link: 'https://www.dndbeyond.com/magic-items/rope-of-entanglement' },
			{
				name: 'Shield of missile attraction',
				link: 'https://www.dndbeyond.com/magic-items/shield-of-missile-attraction'
			},
			{ name: 'Shield, +2', link: 'https://www.dndbeyond.com/magic-items/shield-2' },
			{ name: 'Staff of charming', link: 'https://www.dndbeyond.com/magic-items/staff-of-charming' },
			{ name: 'Staff of healing', link: 'https://www.dndbeyond.com/magic-items/staff-of-healing' },
			{ name: 'Staff of swarming insects', link: 'https://www.dndbeyond.com/magic-items/staff-of-swarming-insects' },
			{ name: 'Staff of the woodlands', link: 'https://www.dndbeyond.com/magic-items/staff-of-the-woodlands' },
			{ name: 'Staff of withering', link: 'https://www.dndbeyond.com/magic-items/staff-of-withering' },
			{
				name: 'Stone of controlling earth elementals',
				link: 'https://www.dndbeyond.com/magic-items/stone-of-controlling-earth-elementals'
			},
			{ name: 'Sun blade', link: 'https://www.dndbeyond.com/magic-items/sun-blade' },
			{ name: 'Sword of life stealing', link: 'https://www.dndbeyond.com/magic-items/sword-of-life-stealing' },
			{ name: 'Sword of wounding', link: 'https://www.dndbeyond.com/magic-items/sword-of-wounding' },
			{ name: 'Tentacle rod', link: 'https://www.dndbeyond.com/magic-items/tentacle-rod' },
			{ name: 'Vicious weapon', link: 'https://www.dndbeyond.com/magic-items/vicious-weapon' },
			{ name: 'Wand of binding', link: 'https://www.dndbeyond.com/magic-items/wand-of-binding' },
			{ name: 'Wand of enemy detection', link: 'https://www.dndbeyond.com/magic-items/wand-of-enemy-detection' },
			{ name: 'Wand of fear', link: 'https://www.dndbeyond.com/magic-items/wand-of-fear' },
			{ name: 'Wand of fireballs', link: 'https://www.dndbeyond.com/magic-items/wand-of-fireballs' },
			{ name: 'Wand of lightning bolts', link: 'https://www.dndbeyond.com/magic-items/wand-of-lightning-bolts' },
			{ name: 'Wand of paralysis', link: 'https://www.dndbeyond.com/magic-items/wand-of-paralysis' },
			{ name: 'Wand of the war mage, +2', link: 'https://www.dndbeyond.com/magic-items/wand-of-the-war-mage-2' },
			{ name: 'Wand of wonder', link: 'https://www.dndbeyond.com/magic-items/wand-of-wonder' },
			{ name: 'Weapon, +2', link: 'https://www.dndbeyond.com/magic-items/weapon-2' },
			{ name: 'Wings of flying', link: 'https://www.dndbeyond.com/magic-items/wings-of-flying' }
		]
	},
	{
		type: 'Very Rare - Minor',
		items: [
			{
				name: 'Potion of Healing (Supreme)',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-healing-supreme',
				alwaysInclude: true
			},
			{ name: 'Ammunition, +3', link: 'https://www.dndbeyond.com/magic-items/ammunition-3' },
			{ name: 'Arrow of slaying', link: 'https://www.dndbeyond.com/magic-items/arrow-of-slaying' },
			{ name: 'Bag of devouring', link: 'https://www.dndbeyond.com/magic-items/bag-of-devouring' },
			{ name: 'Horseshoes of a zephyr', link: 'https://www.dndbeyond.com/magic-items/horseshoes-of-a-zephyr' },
			{ name: 'Nolzur’s marvelous pigments', link: 'https://www.dndbeyond.com/magic-items/nolzurs-marvelous-pigments' },
			{ name: 'Oil of sharpness', link: 'https://www.dndbeyond.com/magic-items/oil-of-sharpness' },
			{
				name: 'Potion of cloud giant strength',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-cloud-giant-strength'
			},
			{ name: 'Potion of flying', link: 'https://www.dndbeyond.com/magic-items/potion-of-flying' },
			{ name: 'Potion of invisibility', link: 'https://www.dndbeyond.com/magic-items/potion-of-invisibility' },
			{ name: 'Potion of longevity', link: 'https://www.dndbeyond.com/magic-items/potion-of-longevity' },
			{ name: 'Potion of speed', link: 'https://www.dndbeyond.com/magic-items/potion-of-speed' },
			{ name: 'Potion of vitality', link: 'https://www.dndbeyond.com/magic-items/potion-of-vitality' },
			{ name: 'Spell scroll (6th level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Spell scroll (7th level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Spell scroll (8th level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' }
		]
	},
	{
		type: 'Very Rare - Major',
		items: [
			{ name: 'Amulet of the planes', link: 'https://www.dndbeyond.com/magic-items/amulet-of-the-planes' },
			{ name: 'Animated shield', link: 'https://www.dndbeyond.com/magic-items/animated-shield' },
			{ name: 'Armor, +2', link: 'https://www.dndbeyond.com/magic-items/armor-2' },
			{
				name: 'Belt of fire giant strength',
				link: 'https://www.dndbeyond.com/magic-items/belt-of-fire-giant-strength'
			},
			{
				name: 'Belt of giant strength (frost/stone)',
				link: 'https://www.dndbeyond.com/magic-items/belt-of-giant-strength'
			},
			{ name: 'Candle of invocation', link: 'https://www.dndbeyond.com/magic-items/candle-of-invocation' },
			{ name: 'Carpet of flying', link: 'https://www.dndbeyond.com/magic-items/carpet-of-flying' },
			{ name: 'Cloak of arachnida', link: 'https://www.dndbeyond.com/magic-items/cloak-of-arachnida' },
			{ name: 'Crystal ball (very rare)', link: 'https://www.dndbeyond.com/magic-items/crystal-ball' },
			{ name: 'Dancing sword', link: 'https://www.dndbeyond.com/magic-items/dancing-sword' },
			{ name: 'Demon armor', link: 'https://www.dndbeyond.com/magic-items/demon-armor' },
			{ name: 'Dragon scale mail', link: 'https://www.dndbeyond.com/magic-items/dragon-scale-mail' },
			{ name: 'Dwarven plate', link: 'https://www.dndbeyond.com/magic-items/dwarven-plate' },
			{ name: 'Dwarven thrower', link: 'https://www.dndbeyond.com/magic-items/dwarven-thrower' },
			{ name: 'Efreeti bottle', link: 'https://www.dndbeyond.com/magic-items/efreeti-bottle' },
			{
				name: 'Figurine of wondrous power (obsidian steed)',
				link: 'https://www.dndbeyond.com/magic-items/figurine-of-wondrous-power'
			},
			{ name: 'Frost brand', link: 'https://www.dndbeyond.com/magic-items/frost-brand' },
			{ name: 'Helm of brilliance', link: 'https://www.dndbeyond.com/magic-items/helm-of-brilliance' },
			{ name: 'Horn of Valhalla (bronze)', link: 'https://www.dndbeyond.com/magic-items/horn-of-valhalla' },
			{
				name: 'Instrument of the bards (Anstruth harp)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{ name: 'Ioun stone (absorption)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (agility)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (fortitude)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (insight)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (intellect)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (leadership)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (strength)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Manual of bodily health', link: 'https://www.dndbeyond.com/magic-items/manual-of-bodily-health' },
			{ name: 'Manual of gainful exercise', link: 'https://www.dndbeyond.com/magic-items/manual-of-gainful-exercise' },
			{ name: 'Manual of golems', link: 'https://www.dndbeyond.com/magic-items/manual-of-golems' },
			{
				name: 'Manual of quickness of action',
				link: 'https://www.dndbeyond.com/magic-items/manual-of-quickness-of-action'
			},
			{ name: 'Mirror of life trapping', link: 'https://www.dndbeyond.com/magic-items/mirror-of-life-trapping' },
			{ name: 'Nine lives stealer', link: 'https://www.dndbeyond.com/magic-items/nine-lives-stealer' },
			{ name: 'Oathbow', link: 'https://www.dndbeyond.com/magic-items/oathbow' },
			{ name: 'Ring of regeneration', link: 'https://www.dndbeyond.com/magic-items/ring-of-regeneration' },
			{ name: 'Ring of shooting stars', link: 'https://www.dndbeyond.com/magic-items/ring-of-shooting-stars' },
			{ name: 'Ring of telekinesis', link: 'https://www.dndbeyond.com/magic-items/ring-of-telekinesis' },
			{
				name: 'Robe of scintillating colors',
				link: 'https://www.dndbeyond.com/magic-items/robe-of-scintillating-colors'
			},
			{ name: 'Robe of stars', link: 'https://www.dndbeyond.com/magic-items/robe-of-stars' },
			{ name: 'Rod of absorption', link: 'https://www.dndbeyond.com/magic-items/rod-of-absorption' },
			{ name: 'Rod of alertness', link: 'https://www.dndbeyond.com/magic-items/rod-of-alertness' },
			{ name: 'Rod of security', link: 'https://www.dndbeyond.com/magic-items/rod-of-security' },
			{ name: 'Rod of the pact keeper, +3', link: 'https://www.dndbeyond.com/magic-items/rod-of-the-pact-keeper-3' },
			{ name: 'Scimitar of speed', link: 'https://www.dndbeyond.com/magic-items/scimitar-of-speed' },
			{ name: 'Shield, +3', link: 'https://www.dndbeyond.com/magic-items/shield-3' },
			{ name: 'Spellguard shield', link: 'https://www.dndbeyond.com/magic-items/spellguard-shield' },
			{ name: 'Staff of fire', link: 'https://www.dndbeyond.com/magic-items/staff-of-fire' },
			{ name: 'Staff of frost', link: 'https://www.dndbeyond.com/magic-items/staff-of-frost' },
			{ name: 'Staff of power', link: 'https://www.dndbeyond.com/magic-items/staff-of-power' },
			{ name: 'Staff of striking', link: 'https://www.dndbeyond.com/magic-items/staff-of-striking' },
			{
				name: 'Staff of thunder and lightning',
				link: 'https://www.dndbeyond.com/magic-items/staff-of-thunder-and-lightning'
			},
			{ name: 'Sword of sharpness', link: 'https://www.dndbeyond.com/magic-items/sword-of-sharpness' },
			{ name: 'Tome of clear thought', link: 'https://www.dndbeyond.com/magic-items/tome-of-clear-thought' },
			{
				name: 'Tome of leadership and influence',
				link: 'https://www.dndbeyond.com/magic-items/tome-of-leadership-and-influence'
			},
			{ name: 'Tome of understanding', link: 'https://www.dndbeyond.com/magic-items/tome-of-understanding' },
			{ name: 'Wand of polymorph', link: 'https://www.dndbeyond.com/magic-items/wand-of-polymorph' },
			{ name: 'Wand of the war mage, +3', link: 'https://www.dndbeyond.com/magic-items/wand-of-the-war-mage-3' },
			{ name: 'Weapon, +3', link: 'https://www.dndbeyond.com/magic-items/weapon-3' }
		]
	},
	{
		type: 'Legendary - Minor',
		items: [
			{
				name: 'Potion of storm giant strength',
				link: 'https://www.dndbeyond.com/magic-items/potion-of-storm-giant-strength'
			},
			{ name: 'Sovereign glue', link: 'https://www.dndbeyond.com/magic-items/sovereign-glue' },
			{ name: 'Spell scroll (9th level)', link: 'https://www.dndbeyond.com/magic-items/spell-scroll' },
			{ name: 'Universal solvent', link: 'https://www.dndbeyond.com/magic-items/universal-solvent' }
		]
	},
	{
		type: 'Legendary - Major',
		items: [
			{ name: 'Apparatus of Kwalish', link: 'https://www.dndbeyond.com/magic-items/apparatus-of-kwalish' },
			{ name: 'Armor of invulnerability', link: 'https://www.dndbeyond.com/magic-items/armor-of-invulnerability' },
			{ name: 'Armor, +3', link: 'https://www.dndbeyond.com/magic-items/armor-3' },
			{
				name: 'Belt of cloud giant strength',
				link: 'https://www.dndbeyond.com/magic-items/belt-of-cloud-giant-strength'
			},
			{
				name: 'Belt of storm giant strength',
				link: 'https://www.dndbeyond.com/magic-items/belt-of-storm-giant-strength'
			},
			{ name: 'Cloak of invisibility', link: 'https://www.dndbeyond.com/magic-items/cloak-of-invisibility' },
			{ name: 'Crystal ball (legendary)', link: 'https://www.dndbeyond.com/magic-items/crystal-ball' },
			{ name: 'Cubic gate', link: 'https://www.dndbeyond.com/magic-items/cubic-gate' },
			{ name: 'Deck of many things', link: 'https://www.dndbeyond.com/magic-items/deck-of-many-things' },
			{ name: 'Defender', link: 'https://www.dndbeyond.com/magic-items/defender' },
			{ name: 'Efreeti chain', link: 'https://www.dndbeyond.com/magic-items/efreeti-chain' },
			{ name: 'Hammer of thunderbolts', link: 'https://www.dndbeyond.com/magic-items/hammer-of-thunderbolts' },
			{ name: 'Holy avenger', link: 'https://www.dndbeyond.com/magic-items/holy-avenger' },
			{ name: 'Horn of Valhalla (iron)', link: 'https://www.dndbeyond.com/magic-items/horn-of-valhalla' },
			{
				name: 'Instrument of the bards (Ollamh harp)',
				link: 'https://www.dndbeyond.com/magic-items/instrument-of-the-bards'
			},
			{ name: 'Ioun stone (greater absorption)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (mastery)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Ioun stone (regeneration)', link: 'https://www.dndbeyond.com/magic-items/ioun-stone' },
			{ name: 'Iron flask', link: 'https://www.dndbeyond.com/magic-items/iron-flask' },
			{ name: 'Luck blade', link: 'https://www.dndbeyond.com/magic-items/luck-blade' },
			{
				name: 'Plate armor of etherealness',
				link: 'https://www.dndbeyond.com/magic-items/plate-armor-of-etherealness'
			},
			{
				name: 'Ring of air elemental command',
				link: 'https://www.dndbeyond.com/magic-items/ring-of-air-elemental-command'
			},
			{ name: 'Ring of djinni summoning', link: 'https://www.dndbeyond.com/magic-items/ring-of-djinni-summoning' },
			{
				name: 'Ring of earth elemental command',
				link: 'https://www.dndbeyond.com/magic-items/ring-of-earth-elemental-command'
			},
			{
				name: 'Ring of fire elemental command',
				link: 'https://www.dndbeyond.com/magic-items/ring-of-fire-elemental-command'
			},
			{ name: 'Ring of invisibility', link: 'https://www.dndbeyond.com/magic-items/ring-of-invisibility' },
			{ name: 'Ring of spell turning', link: 'https://www.dndbeyond.com/magic-items/ring-of-spell-turning' },
			{ name: 'Ring of three wishes', link: 'https://www.dndbeyond.com/magic-items/ring-of-three-wishes' },
			{
				name: 'Ring of water elemental command',
				link: 'https://www.dndbeyond.com/magic-items/ring-of-water-elemental-command'
			},
			{ name: 'Robe of the archmagi', link: 'https://www.dndbeyond.com/magic-items/robe-of-the-archmagi' },
			{ name: 'Rod of lordly might', link: 'https://www.dndbeyond.com/magic-items/rod-of-lordly-might' },
			{ name: 'Rod of resurrection', link: 'https://www.dndbeyond.com/magic-items/rod-of-resurrection' },
			{ name: 'Scarab of protection', link: 'https://www.dndbeyond.com/magic-items/scarab-of-protection' },
			{ name: 'Sphere of annihilation', link: 'https://www.dndbeyond.com/magic-items/sphere-of-annihilation' },
			{ name: 'Staff of the magi', link: 'https://www.dndbeyond.com/magic-items/staff-of-the-magi' },
			{ name: 'Sword of answering', link: 'https://www.dndbeyond.com/magic-items/sword-of-answering' },
			{ name: 'Talisman of pure good', link: 'https://www.dndbeyond.com/magic-items/talisman-of-pure-good' },
			{ name: 'Talisman of the sphere', link: 'https://www.dndbeyond.com/magic-items/talisman-of-the-sphere' },
			{ name: 'Talisman of ultimate evil', link: 'https://www.dndbeyond.com/magic-items/talisman-of-ultimate-evil' },
			{ name: 'Tome of the stilled tongue', link: 'https://www.dndbeyond.com/magic-items/tome-of-the-stilled-tongue' },
			{ name: 'Vorpal sword', link: 'https://www.dndbeyond.com/magic-items/vorpal-sword' },
			{ name: 'Well of many worlds', link: 'https://www.dndbeyond.com/magic-items/well-of-many-worlds' }
		]
	}
];

export default magicItems;

export const getMagicItemsByName = () =>
	([] as { name: string; link: string; type: string }[]).concat(
		...magicItems.map((g) => g.items.map((i) => ({ ...i, type: g.type })))
	);
