import axios from 'axios';
import Character from '../models/Character';
import DdbApiMessage from '../models/DdbApiMessage';
import { getCharacterIds } from './dataStoreApi';

const getCorsWrapper = (url: string) => `https://thingproxy.freeboard.io/fetch/${url}`;
const getDdbApiUrl = (characterId: string) =>
	`https://character-service.dndbeyond.com/character/v3/character/${characterId}`;

export const getStoredCharacters = async () => getCharacters(await getCharacterIds());

export const getCharacters = async (ids: string[]) => await Promise.all(ids.map<Promise<Character>>(getCharacter));

export const getCharacter = async (id: string): Promise<Character> => {
	try {
		const response = await axios.get<DdbApiMessage>(getCorsWrapper(getDdbApiUrl(id)));
		return response.data.data;
	} catch (e) {
		console.log(e);
		throw e;
	}
};

