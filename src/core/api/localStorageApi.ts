import { reactLocalStorage } from 'reactjs-localstorage';
import GridLayoutItem from '../models/GridLayoutItem';

const gridLayoutKey = 'gridLayout';
export const getLayouts = (): ReactGridLayout.Layout[] =>
	reactLocalStorage.getObject(gridLayoutKey) as ReactGridLayout.Layout[];

export const setLayouts = (layouts: ReactGridLayout.Layout[]) => reactLocalStorage.setObject(gridLayoutKey, layouts);
