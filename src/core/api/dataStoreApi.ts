import { DataStore } from 'aws-amplify';
import { Settings } from '../../models';

export const getSettings = async () => (await DataStore.query(Settings))[0];

export const getCharacterIds = async () => (await getSettings())?.characters || [];

export const addCharacters = async (ids: string[]) => {
	const original = await getSettings();
	await DataStore.save(
		Settings.copyOf(original, (u) => {
			u.characters = [...u.characters, ...ids];
		})
	);
};

export const addCharacter = async (id: string) => {
	const original = (await getSettings()) || new Settings({ characters: [] });
	await DataStore.save(
		Settings.copyOf(original, (u) => {
			u.characters = [...u.characters, id];
		})
	);
};

export const deleteCharacter = async (id: string) => {
	const original = await getSettings();
	await DataStore.save(
		Settings.copyOf(original, (u) => {
			u.characters = u.characters.filter((c) => c != id);
		})
	);
};
