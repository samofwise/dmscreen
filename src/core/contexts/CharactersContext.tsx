import * as React from 'react';
import { getStoredCharacters } from '../api/api';
import Character from '../models/Character';

export const CharactersContext = React.createContext<[Character[], React.Dispatch<React.SetStateAction<Character[]>>]>([[], null]);

export const CharactersProvider = ({ children }: React.PropsWithChildren<any>) => {
	const [characters, setCharacters] = React.useState<Character[]>([]);
	React.useEffect(() => {
		const fetch = async () => setCharacters(await getStoredCharacters());
		fetch();
	}, []);

	return <CharactersContext.Provider value={[characters, setCharacters]}>{children}</CharactersContext.Provider>;
};
