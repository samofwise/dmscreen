import Character, { Stat } from './models/Character';

export const getDisplayHpPercent = (c: Character) => getHpPercent(c) - getTempPercent(c);
export const getDisplayMaxPercent = (c: Character) => getMaxPercent(c) - getHpPercent(c);

export const getTempPercent = (c: Character) => (c.temporaryHitPoints / getFullTotalHitpoints(c)) * 100;
export const getHpPercent = (c: Character) => (getCurrentHitpoints(c) / getFullTotalHitpoints(c)) * 100;
export const getMaxPercent = (c: Character) => (getMaxHitpoints(c) / getFullTotalHitpoints(c)) * 100;

export const getCurrentHitpoints = (c: Character) => getMaxHitpoints(c) - c.removedHitPoints;

export const getMaxHitpoints = (c: Character) =>
	(c.overrideHitPoints || c.baseHitPoints) + c.bonusHitPoints + getConstituionHitpoints(c);

export const getFullTotalHitpoints = (c: Character) =>
	(c.overrideHitPoints || c.baseHitPoints) + Math.max(c.bonusHitPoints, 0) + getConstituionHitpoints(c);

export const getConstituionHitpoints = (c: Character) => Math.min(getConstituionMod(c), 1) * getTotalLevel(c);

export const getConstituionMod = (c: Character) => getModifier(c.stats.find((s) => s.id == 3));

export const getModifier = (s: Stat) => (s.value - 10) % 2;

export const getTotalLevel = (c: Character) => c.classes.map((cl) => cl.level).reduce((a, b) => a + b, 0);
