import * as React from 'react';
import { CharactersProvider } from '../core/contexts/CharactersContext';
import Nav from './Nav';
import DmScreen from './pages/DmScreen';
import Amplify from 'aws-amplify';
import { withAuthenticator } from '@aws-amplify/ui-react';
import config from '../aws-exports';
import { createGlobalStyle } from 'styled-components';
import '@fontsource/roboto';
import '@fontsource/roboto-condensed';

Amplify.configure(config);

const App = () => (
	<>
		<GlobalStyle />
		<CharactersProvider>
			<Nav />
			<DmScreen />
		</CharactersProvider>
	</>
);

const GlobalStyle = createGlobalStyle`
body{
	background: #222;
	color: #b0b7bd;
	font-family: Roboto,Helvetica,sans-serif;
	margin: 0;
}
body, #app{
	height: 100%;
	width: 100%;
}
`;

export default withAuthenticator(App);
