import { AmplifySignOut } from '@aws-amplify/ui-react';
import * as React from 'react';
import styled from 'styled-components';

const Nav = () => {
	return (
		<StyledNav>
			<StyledHeader>DM Screen</StyledHeader>
			<StyledSignOut />
		</StyledNav>
	);
};

const StyledNav = styled.nav`
	background: #000;
	overflow: hidden;
	color: #fff;
`;

const StyledHeader = styled.h1`
	text-align: center;
	float: left;
`;

const StyledSignOut = styled(AmplifySignOut)`
	float: right;
`;

export default Nav;
