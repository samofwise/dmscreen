import React from 'react';
import styled from 'styled-components';

const StyledGridItem = ({ children, className }: React.HTMLAttributes<HTMLSpanElement>) => (
	<StyledHealthTracker className={className}>
		<StyledWrapper>{children}</StyledWrapper>
	</StyledHealthTracker>
);

export default StyledGridItem;

const StyledHealthTracker = styled.section`
	overflow: hidden;
	height: calc(100% - 20px);
	margin: 10px;
	border: 1px solid #cf9f25;
	border-radius: 10px;
	background: #242527;
`;

const StyledWrapper = styled.section`
	overflow: auto;
	height: 100%;
	padding: 10px;
`;
