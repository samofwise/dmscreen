import styled from 'styled-components';

export const StyledHeader = styled.h4`
	margin-top: 0;
	text-transform: uppercase;
	font-family: Roboto Condensed, Roboto, Helvetica, sans-serif;
`;
