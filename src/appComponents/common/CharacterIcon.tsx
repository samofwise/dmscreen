import React from 'react';
import styled from 'styled-components';
import Character from '../../core/models/Character';

const CharacterIcon = ({ character: c, className }: Props & React.HTMLAttributes<HTMLSpanElement>) => (
	<ImageWrapper className={className}>
		<IconPortrait backgroundImage={c.avatarUrl} />
		{c.frameAvatarUrl && <IconFrame backgroundImage={c.frameAvatarUrl} />}
	</ImageWrapper>
);

export default CharacterIcon;

interface Props {
	character: Character;
}

const ImageWrapper = styled.span`
	position: relative;
	display: inline-block;
	overflow: hidden;
	width: 50px;
	height: 50px;
`;

interface IconFrameProps {
	backgroundImage: string;
}

const IconImage = styled.span`
	display: block;
	background-image: url(${(p: IconFrameProps) => p.backgroundImage});
	background-size: cover;
	background-repeat: no-repeat;
`;

const IconPortrait = styled(IconImage)`
	border-radius: 50%;
	margin: 15% auto 0;
	width: 58%;
	height: 58%;
`;

const IconFrame = styled(IconImage)`
	position: absolute;
	top: -28%;
	left: 50%;
	transform: translateX(-50%);
	width: 143%;
	height: 129%;
`;
