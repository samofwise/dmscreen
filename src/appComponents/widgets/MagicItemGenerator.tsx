import React from 'react';
import styled from 'styled-components';
import magicItems from '../../core/data/magicItems';
import { StyledHeader } from '../common/Headers';
import StyledGridItem from '../common/StyledGridItem';

const MagicItemGenerator = () => {
	const numberOfItems = 5;
	const magicItemsWithHealthPotions = magicItems.map((g) => ({
		type: g.type,
		items: [
			...[defaultFirstRow.find((r) => r.type == g.type)],
			...getRandomSet(
				g.items.filter((i) => !i.alwaysInclude),
				numberOfItems
			)
		]
	}));

	return (
		<StyledGridItem>
			<StyledHeader>Magic Item Generator</StyledHeader>
			<StyledList>
				{magicItemsWithHealthPotions.map((group) => (
					<StyledGroup>
						<h6>{group.type}</h6>
						{group.items.map((i) => i && <StyledLink href={i.link}>{i.name}</StyledLink>)}
					</StyledGroup>
				))}
			</StyledList>
		</StyledGridItem>
	);
};

export default MagicItemGenerator;

const getRandomSet = <_, T>(array: T[], num: number) => array.sort(() => 0.5 - Math.random()).slice(0, num);

const StyledList = styled.section`
	height: calc(100% - 30px);
	white-space: nowrap;
	width: calc((220px + 10px) * 9);
`;

const StyledGroup = styled.article`
	margin: 0 5px;
	float: left;
	width: 220px;
	white-space: normal;
`;

const StyledLink = styled.a`
	display: block;
	min-height: 1em;
`;

const defaultFirstRow = [
	{
		type: 'Common - Minor',
		name: 'Potion of Healing',
		link: 'https://www.dndbeyond.com/magic-items/potion-of-healing'
	},
	{
		type: 'Uncommon - Minor',
		name: 'Potion of Healing (Greater)',
		link: 'https://www.dndbeyond.com/magic-items/potion-of-healing-greater'
	},
	{
		type: 'Rare - Minor',
		name: 'Potion of Healing (Superior)',
		link: 'https://www.dndbeyond.com/magic-items/potion-of-healing-superior'
	},
	{
		type: 'Very Rare - Minor',
		name: 'Potion of Healing (Supreme)',
		link: 'https://www.dndbeyond.com/magic-items/potion-of-healing-supreme'
	}
];
