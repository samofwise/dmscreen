import React from 'react';
import styled from 'styled-components';
import { CharactersContext } from '../../core/contexts/CharactersContext';
import CharacterIcon from '../common/CharacterIcon';
import { StyledHeader } from '../common/Headers';
import StyledGridItem from '../common/StyledGridItem';
import { CProgress, CProgressBar } from '@coreui/react';
import '@coreui/coreui/dist/css/coreui.min.css';
import {
	getCurrentHitpoints,
	getDisplayHpPercent,
	getDisplayMaxPercent,
	getTempPercent
} from '../../core/characterUtils';

const HealthTracker = () => {
	const [characters] = React.useContext(CharactersContext);

	return (
		<StyledGridItem>
			<StyledHeader>Health Tracker</StyledHeader>
			<StyledList>
				{characters.map((c, i) => (
					<StyledItem key={i}>
						<StyledCharacterIcon character={c} />
						<StyledWrapper>
							<span>{c.name}</span>
							<StyledProgressBar className="mb-3 progress-bar-striped">
								<CProgressBar color="warning" value={getTempPercent(c)}>
									{c.temporaryHitPoints}
								</CProgressBar>
								<CProgressBar color="info" value={getDisplayHpPercent(c)}>
									{getCurrentHitpoints(c)}
								</CProgressBar>
								<CProgressBar color="danger" variant="striped" value={getDisplayMaxPercent(c)} />
							</StyledProgressBar>
						</StyledWrapper>
					</StyledItem>
				))}
			</StyledList>
		</StyledGridItem>
	);
};

export default HealthTracker;

const StyledList = styled.section`
	overflow: auto;
`;

const StyledItem = styled.div`
	overflow: hidden;
	> * {
		float: left;
	}
`;

const StyledCharacterIcon = styled(CharacterIcon)`
	width: 50px;
	height: 50px;
`;

const StyledWrapper = styled.div`
	margin-left: 10px;
	width: calc(100% - 60px);
`;

const StyledProgressBar = styled(CProgress)`
	background-color: var(--cui-progress-bg, #111);
`;
