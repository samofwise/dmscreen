import * as React from 'react';
import styled from 'styled-components';
import { FiPlusCircle, FiEdit3, FiXCircle } from 'react-icons/fi';
import { CharactersContext } from '../../core/contexts/CharactersContext';
import { getCharacter, getCharacters } from '../../core/api/api';
import Character from '../../core/models/Character';
import { addCharacter, addCharacters, deleteCharacter } from '../../core/api/dataStoreApi';
import { StyledHeader } from '../common/Headers';
import CharacterIcon from '../common/CharacterIcon';

const CurrentCharacters = ({ className }: React.HTMLAttributes<HTMLDivElement>) => {
	const [characters] = React.useContext(CharactersContext);
	const [isEditing, setIsEditing] = React.useState(false);

	const toggleEditing = () => setIsEditing((c) => !c);

	const onDeleteCharacter = (c: Character) => {
		alert(`${c.name} has been removed`);
		deleteCharacter(c.id.toString());
	};

	const CharacterItem = ({ character: c }: { character: Character }) => (
		<StyledItem {...(!isEditing ? { href: c.readonlyUrl } : { onClick: () => onDeleteCharacter(c) })}>
			<StyledCharacterIcon character={c} />
			{isEditing && <FiXCircle />}
			<StyledCharacterName>{c.name}</StyledCharacterName>
		</StyledItem>
	);

	return (
		<StyledCurrentCharacters className={className}>
			<StyledHeader>
				Current Characters
				<FiEdit3 onClick={toggleEditing} />
			</StyledHeader>
			{characters.map((c, i) => (
				<CharacterItem character={c} key={i} />
			))}
			{isEditing && <AddCharacterIcon />}
		</StyledCurrentCharacters>
	);
};

export default CurrentCharacters;

const AddCharacterIcon = () => (
	<StyledItem onClick={handleAddClick}>
		<StyledPlusIcon>
			<StyledFiPlusCircle />
		</StyledPlusIcon>
		<StyledCharacterName>Add Character</StyledCharacterName>
	</StyledItem>
);

const getCharacterIdFromUrl = (url: string) => url.replace(/https:\/\/www\.dndbeyond\.com\/.*characters\//, '');

const handleAddClick = async () => {
	const url = prompt(
		'Enter the url of your DndBeyond character.',
		'https://www.dndbeyond.com/profile/samofwise/characters/4338000'
	);
	if (!url) {
		alert('No url detected');
		return;
	}

	const id = getCharacterIdFromUrl(url);
	const character = await getCharacter(id);
	const characterId = character.id.toString();

	if (character) {
		await addCharacter(characterId);

		if (!character.campaign) alert(`${character.name} has been added.`);
		else {
			const additionalCharactersIds = character.campaign.characters
				.map((c) => c.characterId.toString())
				.filter((id) => id != characterId);
				
			if (additionalCharactersIds) {
				const response = confirm(
					`${character.name} has been added.\nOther characters in the campaign "${character.campaign.name}" have been detected.\nWould you like to add them too?`
				);
				if (response) {
					const characters = await getCharacters(additionalCharactersIds);
					await addCharacters(characters.map((c) => c.id.toString()));
					alert(`${characters.map((c) => c.name).join(', ')} have been added.`);
				}
			}
		}
	}
};

const StyledCurrentCharacters = styled.section`
	margin-top: 10px;
`;

const StyledItem = styled.a`
	color: inherit;
	text-decoration: inherit;
	display: inline-block;
	margin: 0 5px;
`;

const StyledCharacterIcon = styled(CharacterIcon)`
	display: block;
	width: 76px;
	height: 76px;
	margin: auto;
`;

const StyledCharacterName = styled.div`
	text-align: center;
`;

const ImageWrapper = styled.div`
	position: relative;
	width: 76px;
	height: 76px;
	margin: auto;
`;

const StyledPlusIcon = styled.span`
	display: block;
	width: 54px;
	height: 54px;
	margin: auto;
`;

const StyledFiPlusCircle = styled(FiPlusCircle)`
	width: 100%;
	height: 100%;
`;
