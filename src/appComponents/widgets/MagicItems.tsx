import React from 'react';
import styled from 'styled-components';
import { CharactersContext } from '../../core/contexts/CharactersContext';
import CharacterIcon from '../common/CharacterIcon';
import { StyledHeader } from '../common/Headers';
import StyledGridItem from '../common/StyledGridItem';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Character from '../../core/models/Character';
import InventoryItem from '../../core/models/InventoryItem';
import magicItems, { getMagicItemsByName } from '../../core/data/magicItems';

const MagicItems = () => {
	const [characters] = React.useContext(CharactersContext);

	return (
		<StyledGridItem>
			<StyledHeader>Magic Items</StyledHeader>
			<Tabs>
				<TabList>
					<Tab>List</Tab>
					<Tab>Tracker</Tab>
				</TabList>
				<TabPanel>
					<MagicItemList />
				</TabPanel>
				<TabPanel>
					<MagicItemTracker />
				</TabPanel>
			</Tabs>
		</StyledGridItem>
	);
};

export default MagicItems;

const MagicItemList = () => {
	const [characters] = React.useContext(CharactersContext);

	const magicItems = ([] as { character: string; item: InventoryItem; link: string }[]).concat(
		...characters.map((c) =>
			getMagicItems(c).map((i) => ({ character: c.name, item: i, link: getMagicItemFromList(i.definition.name)?.link }))
		)
	);

	return (
		<StyledList>
			{magicItems.map((it, i) => (
				<div key={i}>
					{it.link ? <a href={it.link}>{it.item.definition.name}</a> : it.item.definition.name} - {it.character}
				</div>
			))}
		</StyledList>
	);
};

const MagicItemTracker = () => {
	const [characters] = React.useContext(CharactersContext);

	return (
		<StyledList>
			{characters.map((c, i) => (
				<div key={i}>
					<CharacterIcon character={c} />
				</div>
			))}
		</StyledList>
	);
};
const StyledList = styled.section`
	overflow: auto;
`;

const getMagicItems = (c: Character) => c.inventory.filter((i) => i.definition.magic);

const getMagicItemFromList = (name: string) => {
	console.log(
		'test',
		name,
		getMagicItemsByName().find((m) => m.name.toUpperCase() == name.toUpperCase())
	);
	return getMagicItemsByName().find((m) => m.name.toUpperCase() == name.toUpperCase()) || null;
};
