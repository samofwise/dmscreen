import * as React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import CurrentCharacters from '../widgets/CurrentCharacters';
import GridLayout from 'react-grid-layout';
import HealthTracker from '../widgets/HealthTracker';

import '/node_modules/react-grid-layout/css/styles.css';
import '/node_modules/react-resizable/css/styles.css';
import MagicItems from '../widgets/MagicItems';
import { getLayouts, setLayouts } from '../../core/api/localStorageApi';
import { CharactersContext } from '../../core/contexts/CharactersContext';
import MagicItemGenerator from '../widgets/MagicItemGenerator';

const DmScreen = () => {
	const [characters, setCharacters] = React.useContext(CharactersContext);
	console.log('Characters', characters);
	const testLayouts = getLayouts();

	const onLayoutChange = (layouts: GridLayout.Layout[]) => {
		setLayouts(layouts);
	};

	return (
		<div>
			<RezisableHandleStyle />
			<StyledCurrentCharacters />
			<GridLayout layout={testLayouts} cols={12} rowHeight={30} width={1200} onLayoutChange={onLayoutChange}>
				<StyledItem key="health-tracker">
					<HealthTracker />
				</StyledItem>
				<StyledItem key="magic-items">
					<MagicItems />
				</StyledItem>
				<StyledItem key="magic-item-generator">
					<MagicItemGenerator />
				</StyledItem>
			</GridLayout>
		</div>
	);
};

const RezisableHandleStyle = createGlobalStyle`
.react-grid-item > .react-resizable-handle::after {
	border-right: 2px solid rgba(255, 255, 255, 0.4);
	border-bottom: 2px solid rgba(255, 255, 255, 0.4);

}
`;

const StyledCurrentCharacters = styled(CurrentCharacters)`
	// float: right;
	overflow: hidden;
`;

const StyledItem = styled.section`
	overflow: visible;
`;

export default DmScreen;
