/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateSettings = /* GraphQL */ `
  subscription OnCreateSettings($owner: String!) {
    onCreateSettings(owner: $owner) {
      id
      characters
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateSettings = /* GraphQL */ `
  subscription OnUpdateSettings($owner: String!) {
    onUpdateSettings(owner: $owner) {
      id
      characters
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteSettings = /* GraphQL */ `
  subscription OnDeleteSettings($owner: String!) {
    onDeleteSettings(owner: $owner) {
      id
      characters
      createdAt
      updatedAt
      owner
    }
  }
`;
